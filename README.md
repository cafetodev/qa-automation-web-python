# Selenium Boxia

This is code is for learning purposes.

### Pre-requirements 📋

* [Python v3.8.6](https://www.python.org/) or higher

### Installation 🔧

1. Install poetry

        pip install poetry

2. Clone repository:

        git clone https://bitbucket.org/cafetodev/qa-automation-web-python/

3. Go to the project folder:

        cd <Project folder name>

4. Install requirements:

        poetry install

5. Run selenium grid

        docker run --rm --name selenium-docker -p 4444:4444 \
        -v ${PWD}/grid-config.toml:/opt/bin/config.toml \
        -v ${PWD}/assets:/opt/selenium/assets \
        -v /var/run/docker.sock:/var/run/docker.sock \
        selenium/standalone-docker
