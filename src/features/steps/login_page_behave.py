from behave import given, then, when
from behave.runner import Context


@given("user is in the login page and fill the form with the <email> and <password>")
def step_impl(context: Context) -> None:
    for row in context.table:
        context.login_page.login(email=row["email"], password=row["password"])


@when("user send a click action to the login button")
def step_impl(context: Context) -> None:
    context.login_page.click_login_button()


@then("error modal popup is displayed")
def step_impl(context: Context) -> None:
    is_not_success_login = context.login_page.is_not_success_login()
    assert is_not_success_login is True
