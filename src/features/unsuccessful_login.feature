# Created by esteban at 8/04/22
Feature: Unsuccessful Login to Boxia portal

  # Enter feature description here
  Scenario: unsuccessful loging should return an error modal
    Given user is in the login page and fill the form with the <email> and <password>
      | email               | password          |
      | some_email@mail.com | se3cret_passw0rd  |
    When user send a click action to the login button
    Then error modal popup is displayed
