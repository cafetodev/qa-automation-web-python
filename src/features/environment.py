"""
Configuration file for behave
"""
import json
import os

from selenium import webdriver

from src.features.lib.login_page import LoginPage

GRID_HUB_URL = os.environ.get("GRID_HUB_URL")
WEBSITE_URL = os.environ.get("WEBSITE_URL")

INDEX = int(os.environ["INDEX"]) if "INDEX" in os.environ else 0

desired_cap_dict = os.environ["LT_BROWSERS"]

CONFIG = json.loads(desired_cap_dict)


def before_feature(context, feature):
    """Set driver configuration and loads a login_page
        pom instance as attributes to context to run all over behave tests for each feature
    """
    desired_cap = setup_desired_cap(CONFIG[INDEX])

    context.driver = webdriver.Remote(
        command_executor=GRID_HUB_URL,
        desired_capabilities=setup_desired_cap(desired_cap),
    )
    context.driver.get(WEBSITE_URL)
    context.login_page = LoginPage(driver=context.driver)


def after_feature(context, feature):
    """Close all drivers instance after test after each feature
    """
    context.driver.quit()


def setup_desired_cap(desired_cap):
    """
    sets the capability according to LT
    :param desired_cap:
    :return:
    """
    return desired_cap
