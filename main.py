"""
Main execution file for Selenium Behave project
"""

import json
import os
import sys
from configparser import ConfigParser
from datetime import datetime
from os import environ, listdir
from os.path import join
from subprocess import run

CONFIG_FILE = "config.ini"  # Config file path
FEATURES_FILES_PATH = "src/features/"  # Feature files folder path


def read_configuration() -> ConfigParser:
    """Reads the config.ini file
    :return ConfigParser
    """
    config_parser = ConfigParser()
    if config_parser.read(CONFIG_FILE):
        return config_parser
    sys.exit("config.cfg file not found.")


def set_selenium_environ(config: ConfigParser) -> None:
    """Sets GRID_HUB_URL environment variable
    :param ConfigParser config: configuration parser serialized into a dictionary
    """
    protocol = config.get("selenium_grid", "protocol") or "http"
    host = config.get("selenium_grid", "url") or "localhost"
    port = config.get("selenium_grid", "port") or "4444"

    url = protocol + "://" + host + ":" + port + "/wd/hub"
    os.environ["GRID_HUB_URL"] = url


def set_browsers_environ(config: ConfigParser):
    """Sets LT_BROWSERS environment variable
    :param ConfigParser config: configuration parser serialized into a dictionary
    """
    browsers_config = config.get("browsers", "browser_list")
    browsers_list = browsers_config.split(", ")
    browsers_info = [
        {"browserName": browser, "version": "latest"} for browser in browsers_list
    ]

    os.environ["LT_BROWSERS"] = json.dumps(browsers_info)


def set_website_environ(config: ConfigParser):
    """Sets WEBSITE_URL environment variable
    :param ConfigParser config: configuration parser serialized into a dictionary
    """
    website = config.get("website", "login")
    os.environ["WEBSITE_URL"] = website


def get_features_files() -> list:
    """Read the the folder path where the .feature files are locates
    and list them

    :return: List of .feature files
    """
    features_files_path = FEATURES_FILES_PATH

    features_files = [
        join(features_files_path, file)
        for file in listdir(features_files_path)
        if file.endswith(".feature") or file.endswith(".feature")
    ]

    return features_files


def run_tests():
    """Runs behave command for all the .feature files and test them with all required browsers.
    Writes an output with the result for each test and browser"""
    pool = json.loads(os.getenv("LT_BROWSERS"))
    features_files = get_features_files()

    for index, item in enumerate(pool):
        environ["INDEX"] = str(index)
        browser_name = item.get("browserName")
        now = datetime.now()
        for file in features_files:
            date_time = now.strftime("%d%m%Y_%H%M%S")
            output_file_name = (
                f"{file.split('/')[-1].replace('.', '_')}_{browser_name}_{date_time}"
            )
            run(
                [
                    "behave",
                    file,
                    "-f",
                    "json",
                    "-o",
                    f"results/{output_file_name}.json",
                ]
            )


if __name__ == "__main__":
    configuration = read_configuration()
    set_selenium_environ(configuration)
    set_browsers_environ(configuration)
    set_website_environ(configuration)
    run_tests()
